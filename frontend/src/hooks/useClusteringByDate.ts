import { useMemo } from "react";

export function useClusteringByDate(dateArray: Date[], clustering: { time: Date }[]) {
    const arr = useMemo(() => {
        const countsArray = [];

        for (let i = 0; i < dateArray.length - 1; i++) {
            const startDate = dateArray[i];
            const endDate = dateArray[i + 1];
            let count = 0;

            for (const obj of clustering) {
                const regTime = obj.time;

                if (startDate <= regTime && regTime < endDate) {
                    count++;
                }
            }

            countsArray.push(count);
        }

        return countsArray
    }, [dateArray, clustering])

    return arr;
}