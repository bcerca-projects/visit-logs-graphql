import { useMemo } from "react";

export function useObjectToTable<T extends Object>(objects: T[] = []) {
    const table = useMemo(() => {
        let data: string[][] = [];
        let header: string[] = []
        if (!objects?.length) {
            return { header, data };
        }
        header = Object.keys(objects[0])
        for (const object of objects) {
            const row: string[] = [];

            for (const property in object) {
                if (object.hasOwnProperty(property)) {
                    row.push(String(object[property]));
                }
            }

            data.push(row);
        }

        return { header, data };
    }, [objects])
    return table
}