// useRequest.js
import { useQuery } from "react-query";
import { GraphQLClient, gql } from "graphql-request";
import { EventLogModels, VisitLogModels } from '@models/activity-logs';
import {config} from "../../config"

const graphQLClient = new GraphQLClient(config().API_URL, {
    headers: {
        Authorization: `Bearer ${process.env.API_KEY}`
    }
});

export function useGetVisitLog(props: { uid: string }) {
    const { uid } = props;
    return useQuery("visitLog", async () => {
        const { visitLog } = await graphQLClient.request < {visitLog: VisitLogModels.IVisitLog}>(gql`
        query VisitLog($uid: ID!) {
            visitLog(uid: $uid) {
                uid
                reg_time
                fc_imp_chk
                fc_time_chk
                utmtr
                mm_dma
                osName
                model
                hardware
                site_id
            }
        }
    `, {uid});
        return visitLog;
    });
}


export function useGetVisitLogPage(props: { skip: number, take: number }) {
    const { skip, take } = props;
    return useQuery(["visitLog",skip,take], async () => {
        const { visitLogList } = await graphQLClient.request<{ visitLogList: VisitLogModels.IVisitLog[] }>(gql`
       query VisitLog($skip: Int!, $take: Int!) {
            visitLogList(skip: $skip, take: $take) {
                reg_time
                uid
                fc_imp_chk
                fc_time_chk
                utmtr
                mm_dma
                osName
                model
                hardware
                site_id
            }
        }
    `, { skip, take });
        return visitLogList;
    });
}
