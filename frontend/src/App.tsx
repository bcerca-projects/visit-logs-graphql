
import './App.scss';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import { VisitLogsComponent, EventLogsComponent } from './pages';

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <VisitLogsComponent></VisitLogsComponent>
    ),
  },
  {
    path: "events",
    element: (<EventLogsComponent></EventLogsComponent>),
  },
]);
function App() {

  return (
    <div className="container">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
