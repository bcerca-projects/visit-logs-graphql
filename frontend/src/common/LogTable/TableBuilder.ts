import { ColWidthEnum } from "common/LogTable/ColWithEnum.enum";

export class TableBuilder<T extends { [key in string]: any }> {

    static create<T extends {[key in string]: any}>(): TableBuilder<T> {
        return new TableBuilder()
    }

    private _cols: { name: keyof T, width: ColWidthEnum }[] = [];

    private _rows: T[] = []

    private constructor(object?: T) {
        const keys = (Object.keys(object ?? {}));
        keys.forEach((key) => {
            this._cols.push({name: key as keyof T, width: ColWidthEnum.MINIMIZE})
        })
    }

    addRows(rows: T[]) {
        this._rows.push(...rows);
        return this;
    }

    setWidth(column: keyof T, width: ColWidthEnum) {
        const col = this._cols.find(col => col.name === column)
        if (!col) {
            throw new Error("Column has't been found")
        }
        col.width = width;
        return this;
     }

    render() {
    }


}