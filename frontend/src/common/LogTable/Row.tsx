import styled from "@emotion/styled";
import { Col } from "./Col";
import { ReactNode } from "react";
import { ColWidthEnum } from "./ColWithEnum.enum";
interface IProps<T extends string> {
  cols?: { value: T, width?: ColWidthEnum }[],
  children?: ReactNode,
}


const TableRow = styled.div`
`;
export function Row<T extends string>(props: IProps<T>) {
  const { cols = [], children } = props;
  return <TableRow className="row m-0 g-0">{cols.map((col, i) => (<Col key={i} {...col} ></Col>))}{children}</TableRow>
}