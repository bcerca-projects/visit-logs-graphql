export enum ColWidthEnum {
    MAXIMIZE = 'col-auto',
    MINIMIZE = 'col'
}
