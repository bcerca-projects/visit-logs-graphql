import { ReactNode } from "react";
import { ColWidthEnum } from "./ColWithEnum.enum"
import styled from "@emotion/styled";

interface IProps<T extends string> {
  value?: T,
  width?: ColWidthEnum,
  children?: ReactNode,
  className?: string,
}
const TableCell = styled.div`
  padding: 10px;
  border-width: 0.5px;
  border-color: #D3D3D3;
  border: solid;
   color: #6C6C6C;
    font-family: Raleway;
    font-style: normal;
    font-weight: 600;
    line-height: normal;
    overflow-x: scroll;
    overflow-y: hidden;
`;


export function Col<T extends string>(props: IProps<T>) {
  const { width, value, children, className: externalClassName } = props;
  const className = `${externalClassName ?? width ?? ColWidthEnum.MINIMIZE}`
  return <TableCell className={className}>{value} {children}</TableCell>
}