import { memo } from "react"
import styled from '@emotion/styled'
import { ColWidthEnum } from "common/LogTable/ColWithEnum.enum";
import { Row } from "./Row";

const TableHeader = styled.div`
  font-weight: bold;

`;


interface IProps {
    cols: {value: string, width?: ColWidthEnum}[]
}



export const Header = memo((props: IProps) => {
    const cols = props;
    return <TableHeader>
        <Row {...cols}></Row>
    </TableHeader>
})