import { Header } from "./Header"
import { Row } from "./Row"

interface IProps<T extends string,>{
    header: string[],
    data: T[][],
    className?: string,
}

export function LogTableComponent<T extends string>(props: IProps<T >)  {
    const { header, data, className = '' } = props
    return <div className={className}>
        <Header cols={header.map(h => ({ value: h }))}></Header>
        {data.map((row,i) => <Row key={i} cols={ row.map(col => ({value: col}))}></Row>)}
    </div>
}