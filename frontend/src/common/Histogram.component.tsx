import { Chart, ChartType } from "chart.js";
import { useEffect, useMemo, useRef } from "react";

export const ChartComponent = (props: {
    labels: string[],
    header: string,
    type?: ChartType,
    data?: (number)[] | (string)[]
}) => {

    const { labels, type = 'line', data = [], header } = props;

    const canvas = useRef<HTMLCanvasElement>(null)

    const chart = useMemo(() => {
        const current = canvas?.current;
        if (!current) {
            return;
        }
        const ctx = current.getContext("2d");
        if (!ctx) {
            return;
        }
        return new Chart(ctx, {
            type,
            data: {
                labels,
                datasets: [{
                    label: header,
                    data,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }, [canvas])

    useEffect(() => {
        if (!chart) {
            return;
        }
        chart.render()
    }, [data])


    return <canvas ref={canvas}></canvas>
}