import { ChangeEventHandler, useCallback, useEffect, useMemo, useState } from "react";
import { FieldComponent } from "./Field.component";

interface IProps<T extends {[key in string]: any}> {
    map: T,
    onChange?: (value: T) => void,
}
export function PropsAdjustingLayer<T extends { [key in string]: any }>(props: IProps<T>) {
    const { map, onChange: onChange = (v) => {} } = props;
    const keys = useMemo(() =>Object.keys(map),[]);
    const [currentMap,setMap ] = useState(map)
    const onChangeCallback = useCallback((props: { event: React.ChangeEvent<HTMLInputElement>, key: string }) => {
        const { event, key } = props;
        const newValue = event.target.value;
        const newMap = { ...currentMap, [key]: newValue }
        setMap(newMap)
    }, [])
    useEffect(() => {
        onChange(currentMap)
    },[currentMap])
    return <>    {keys.map((key, i) => {
        return <FieldComponent key={i} header={key}></FieldComponent>
        }) }
    </>
}