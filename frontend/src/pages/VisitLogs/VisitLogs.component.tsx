import { css } from "@emotion/css";
import { useGetVisitLogPage } from "api/activity-logs";
import { ChartType } from "chart.js";
import { ChartComponent } from "common/Histogram.component";
import { LoaderComponent } from "common/Loader.component";
import { LogTableComponent } from "common/LogTable";
import { PropsAdjustingLayer } from "common/PropsAdjustingLayer.component";
import { useClusteringByDate } from "hooks/useClusteringByDate";
import { useObjectToTable } from "hooks/useObjectToTable";
import moment from "moment";
import { useState } from "react";

const MaxTableHeigth = css`
    max-height: calc(80%);
`

export const VisitLogsComponent = () => {
    const [skip, setSkip] = useState(0);
    const [take, setTake] = useState(100);
    const { isError, isLoading, data: logs, } = useGetVisitLogPage({ skip, take });
    const table = useObjectToTable(logs)
    if (isLoading) {
        return <LoaderComponent></LoaderComponent>
    }
    if (isError || !logs) {
        return <>{logs}</>
    }
    const chartProps: {
        labels: string[],
        header: string,
        type: ChartType,
        data: number[]
    } = {
        labels: [],
        header: "",
        type: "bar",
        data: [],
    }
    const proccesedLogs = logs
        .filter(v => !!v.reg_time)
        .sort((a, b) => moment(a.reg_time, "DD/MM/YYYY HH:mm:ss").valueOf() - moment(b.reg_time, "DD/MM/YYYY HH:mm:ss").valueOf())
    chartProps.labels = proccesedLogs.map(v => moment(v.reg_time, "DD/MM/YYYY HH:mm:ss").toISOString())
    const datePoints = proccesedLogs.map(v => moment(v.reg_time, "DD/MM/YYYY HH:mm:ss").toDate());
    const clusteredLogs = useClusteringByDate(datePoints, datePoints.map(time => ({ time })))

    return <>
        <div className="row">
            <ChartComponent {...chartProps} />
        </div>
        <div className="row">
            <div className="col">
                <LogTableComponent className={MaxTableHeigth} {...table}></LogTableComponent>
            </div>
            <div className="col-2">
                <PropsAdjustingLayer map={logs?.[0]}></PropsAdjustingLayer>
            </div>
        </div>

    </>
}