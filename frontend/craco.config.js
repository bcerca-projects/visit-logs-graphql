/* craco.config.js */
const path = require(`path`);

module.exports = {
    webpack: {
        alias: {
            '@shared/*': path.resolve(__dirname, '../shared/'),
            '@models/*': path.resolve(__dirname, '../shared/models/'),
            '@common/*': path.resolve(__dirname, './src/common/'),
            '@pages/*': path.resolve(__dirname, './src/pages/'),
        }
    },
};