// type Grow<T, A extends Array<T>> = ((x: T, ...xs: A) => void) extends ((...a: infer X) => void) ? X : never;
// type GrowToSize<T, A extends Array<T>, N extends number> = { 0: A, 1: GrowToSize<T, Grow<T, A>, N> }[A['length'] extends N ? 0 : 1];

// export type FixedArray<T, N extends number> = GrowToSize<T, [], N>;

// type ArrayLengthMutationKeys = 'splice' | 'push' | 'pop' | 'shift' | 'unshift'
// export type FixedArray<T, L extends number, TObj = [T, ...Array<T>]> =
//     Pick<TObj, Exclude<keyof TObj, ArrayLengthMutationKeys>>
//     & {
//         readonly length: L
//         [I: number]: T
//         [Symbol.iterator]: () => IterableIterator<T>
//     }


// type Shift<A extends Array<any>> = ((...args: A) => void) extends ((...args: [A[0], ...infer R]) => void) ? R : never;

// type GrowExpRev<A extends Array<any>, N extends number, P extends Array<Array<any>>> = A['length'] extends N ? A : {
//     0: GrowExpRev<[...A, ...P[0]], N, P>,
//     1: GrowExpRev<A, N, Shift<P>>
// }[[...A, ...P[0]][N] extends undefined ? 0 : 1];

// type GrowExp<A extends Array<any>, N extends number, P extends Array<Array<any>>> = A['length'] extends N ? A : {
//     0: GrowExp<[...A, ...A], N, [A, ...P]>,
//     1: GrowExpRev<A, N, P>
// }[[...A, ...A][N] extends undefined ? 0 : 1];

// export type FixedArray<T, N extends number> = N extends 0 ? [] : N extends 1 ? [T] : GrowExp<[T, T], N, [[T]]>;


// export type FixedArray<T, N extends number, M extends string = '0'> = {
//     readonly [k in M]: any;
// } & { length: N } & ReadonlyArray<T>;

export type FixedArray<T, N extends number> = N extends 0 ? never[] : {
    0: T;
    length: N;
} & ReadonlyArray<T>;