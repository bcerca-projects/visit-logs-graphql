
export enum CustomConsolePropsEnum {

       debug = 'debug',

       error = 'error',

       info = 'info',

       log = 'log',

       warn = 'warn',

}


export class CustomConsole {

       private prefixes: {
              [key in CustomConsolePropsEnum]?: string
       }


       private postfix: {
              [key in CustomConsolePropsEnum]?: string
       }

       private context: string;

       constructor() {
              this.context = 'console';
              this.prefixes = {};
              this.postfix = {};
       }

       setContext(context: string): this {
              this.context = context;
              return this;
       }

       setPrefix(func: CustomConsolePropsEnum, prefix: string): this {
              this.prefixes[func] = prefix;
              return this;
       }

       setPostfix(func: CustomConsolePropsEnum, prefix: string): this {
              this.postfix[func] = prefix;
              return this;
       }

       out(func: CustomConsolePropsEnum, msg: string): this {
              // this.context = arguments.callee.caller.toString();
              this.provide(func, msg);
              return this;
       }

       private get timePrefix(): string {
              return (new Date()).toUTCString();
       }

       private provide(func: CustomConsolePropsEnum, msg: string): void {
              const prefix = this.prefixes[func] || '';
              const postfix = this.postfix[func] || '';
              const out = `[${prefix}${this.timePrefix}][${func.toUpperCase()}][${this.context}] ${msg} ${postfix}`;
              if (!console[func]) {
                     console.log(out);
              }
              console[func](out);
       }

       public copy(): CustomConsole {
              const nc = new CustomConsole();
              for (const key in this.prefixes) {
                     if (Object.prototype.hasOwnProperty.call(this.prefixes, key)) {
                            const element = this.prefixes[key];
                            nc.setPrefix(key as CustomConsolePropsEnum, element);
                     }
              }
              for (const key in this.postfix) {
                     if (Object.prototype.hasOwnProperty.call(this.postfix, key)) {
                            const element = this.postfix[key];
                            nc.setPostfix(key as CustomConsolePropsEnum, element);
                     }
              }
              return nc;
       }
}