
export interface IStateCommand {
    type: 'get' | 'change' | 'error'
}

export interface IChangeStateCommand extends IStateCommand {
    name: string,
    value: any,
}

export interface IGetStateCommand extends IStateCommand {
    name: string,
}

export type ChangeStateCommand = {
    type: 'change',
    value: any,
    name: string,

}
export type GetStateCommand = {
    type: 'get',
    name: string,
}

export type StateCommand = ChangeStateCommand | GetStateCommand;