import { UserRole } from "./UserRole";

export interface IUser {
    uuid?: string;
    login?: string;
    password?: string;
    role?: UserRole;
    refreshToken?: string;
}