export enum UserRole {
    client = 'client',
    employee = 'employee',
    moderator = 'moderator',
    administrator = 'administrator',
    super = 'super',
    device = 'device'
}