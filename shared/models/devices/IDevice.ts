import { IUser } from "../users/IUser";
import { UserRole } from "../users/UserRole";



export interface IDevice extends IUser {
    uuid: string;
    role: UserRole.device,
    connected: boolean;
    pin: string;
    creationDate: number;
    expirationDate: number;
    liveToken?: string;
    accuracy?: number;
    altitude?: number | null;
    altitudeAccuracy?: number | null;
    heading?: number | null;
    latitude?: number;
    longitude?: number;
    speed?: number | null;
}