import { EventLogTag } from "./EventLogTag.type";

export interface IEventLog {
    uid: string,
    tag?: EventLogTag,
}