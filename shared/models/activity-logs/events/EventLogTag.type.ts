enum DeterminatedTagEnum {
    CLICK = "click",
    REGISTRATION = "registration",
    CONTENT = "content",
    SIGN_UP = "signup",
    LEAD = "lead"
}

type DeterminatedEvent = `${DeterminatedTagEnum}`
type DeterminatedFirstEvent = `f${DeterminatedTagEnum}`
type DeterminatedViewTroughEvent = `v${DeterminatedTagEnum}`

export type EventLogTag = DeterminatedEvent | DeterminatedFirstEvent | DeterminatedViewTroughEvent