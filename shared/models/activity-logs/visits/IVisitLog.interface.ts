import { IEventLog } from "../events";

export interface IVisitLog {
    reg_time?: string;
    uid?: string;
    fc_imp_chk?: number;
    fc_time_chk?: number;
    utmtr?: number;
    mm_dma?: number;
    osName?: string;
    model?: string;
    hardware?: string;
    site_id?: string;

    /**
     * Child instances
     */
    events?: IEventLog[]
}
