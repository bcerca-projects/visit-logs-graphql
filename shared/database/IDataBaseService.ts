interface IQuery {
    text: string;
    parameters?: any;
}

export interface IDataBaseService {
    connect: () => Promise<this>,
    query: (query: IQuery) => Promise<any>
    destroy: () => void,
}