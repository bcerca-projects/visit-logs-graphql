import { CanActivate, ExecutionContext, Inject, Injectable, mixin, UnauthorizedException, UseGuards } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IUser } from '@shared/models/users/IUser';
import { UserRole } from '@shared/models/users/UserRole';
import { config } from 'config';
import { Request } from 'express';
import { IPayload } from '../auth/IPayload';
import { UsersQueryservice } from '../queries/neo4j/users/usersQuery.service';

export const AuthGuard = (roles?: UserRole[], exceptions?: UserRole[]) => {
    @Injectable()
    class AuthGuardMixin implements CanActivate {
        constructor(
            protected jwtService: JwtService,
            @Inject('usersQuery') private readonly userQuery: UsersQueryservice,
        ) { }
        async canActivate(context: ExecutionContext): Promise<boolean> {
            if (config().god_mode) { return true }
            const ctx = context.switchToHttp();
            const req: Request = ctx.getRequest();
            const header = req.header('authorization').replace('Bearer ', '');
            if (!header || header === 'null') {
                throw new UnauthorizedException();
            }
            const token: IPayload = this.jwtService.verify(header, {
                secret: config().accessTokenSecret,
            }) as IPayload;
            const currentUser = (await this.userQuery.list({
                skip: 0,
                orderProp: 'uuid',
                orderType: 'DESC',
                limit: 1,
                match: { uuid: token.uuid } as IUser,
            }))[0];
            // @ts-ignore
            req.currentUser = currentUser;
            if (roles?.length && !roles.includes(currentUser.role) && exceptions.includes(currentUser.role)) {
                throw new UnauthorizedException();
            }
            if (exceptions?.length && exceptions.includes(currentUser.role)) {
                throw new UnauthorizedException();
            }
            if (!token) {
                throw new UnauthorizedException();
            }
            return true;
        }
    }
    return UseGuards(mixin(AuthGuardMixin));
};
