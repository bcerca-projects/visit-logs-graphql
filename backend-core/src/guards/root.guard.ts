import { CanActivate, ExecutionContext, Injectable, mixin, UnauthorizedException, UseGuards } from "@nestjs/common"
import { JwtService } from "@nestjs/jwt"
import { Request } from 'express'
import { IPayload } from "../auth/IPayload";

export const Root = () => {
    @Injectable()

    class RootGuard implements CanActivate {
        constructor(
            protected jwtService: JwtService,
        ) { }
        async canActivate(context: ExecutionContext): Promise<boolean> {
            const ctx = context.switchToHttp();
            const req: Request = ctx.getRequest();
            const header = req.header('authorization').replace('Bearer ', '');
            if (!header || header === "null") {
                throw new UnauthorizedException();
            }

            const token: IPayload = this.jwtService.verify(header) as IPayload;
            if (token.login !== "root") {
                throw new UnauthorizedException();
            }

            return true;
        }
    }
    return UseGuards(mixin(RootGuard));
}