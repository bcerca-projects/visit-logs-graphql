import { CanActivate, ExecutionContext, Injectable, mixin, UnauthorizedException, UseGuards } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { IPayload } from '../auth/IPayload';

export const WSAuthGuard = () => {
    @Injectable()
    class AuthGuard implements CanActivate {
        constructor(
            protected jwtService: JwtService,
        ) { }
        async canActivate(context: ExecutionContext): Promise<boolean> {
            const ctx = context.switchToWs();

            try {
                const token = ctx.getData()?.accessToken;
                if (!token || token === 'null') {
                    throw new UnauthorizedException();
                }
                const payload: IPayload = this.jwtService.verify(token) as IPayload;
                if (!payload) {
                    throw new UnauthorizedException();
                }
                return true;
            } catch {
                ctx.getClient().send('expired');
            }
        }

    }
    return UseGuards(mixin(AuthGuard));
}