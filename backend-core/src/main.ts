import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { CustomConsole, CustomConsolePropsEnum } from '@shared/console/CustomConsole';
import { LoggerService } from '@nestjs/common';
import { config } from './config';
import { stringify } from 'uuid';


export class MyLogger extends CustomConsole implements LoggerService {


  constructor() {
    super();
    this.setPrefix(CustomConsolePropsEnum.error, '\x1b[41m');
    this.setPostfix(CustomConsolePropsEnum.error, '\x1b[0m');
    this.setPrefix(CustomConsolePropsEnum.log, '\x1b[1m');
    this.setPostfix(CustomConsolePropsEnum.log, '\x1b[0m');
  }

  log(message: string) {
    this.out(CustomConsolePropsEnum.log, `${message}`);
  }
  error(message: string, trace: string) {
    this.out(CustomConsolePropsEnum.error, message);
  }
  warn(message: string) {
    this.out(CustomConsolePropsEnum.warn, message);
  }
  debug(message: string) {
    this.out(CustomConsolePropsEnum.debug, message);
  }
  verbose(message: string) {
    this.out(CustomConsolePropsEnum.log, message);
  }
}

async function bootstrap() {
  const _config = config();
  const app = await NestFactory.create(AppModule, {
    logger: new MyLogger(),
  });
  console.info('Env:', JSON.stringify(_config));
  app.enableCors()
  await app.listen(_config.port);
}
bootstrap();
