
import { Body, ConflictException, Controller, Delete, Get, Inject, Param, ParseIntPipe, ParseUUIDPipe, Post, Put, Query } from '@nestjs/common';
import { IUser } from '@shared/models/users/IUser';
import { UserRole } from '@shared/models/users/UserRole';
import * as uuid from 'uuid';
import * as md5 from 'md5';
import { IsEmail, IsEmpty, IsEnum, IsNotEmpty, IsOptional, IsString, IsUUID } from 'class-validator';
import { CurrentUser } from '../../param-decorators/current-user.decorator';
import { UsersQueryservice } from '../../queries/neo4j/users/usersQuery.service';
import { AuthGuard } from 'guards/auth.guard';
import { CreateUserDto, UpdateUserDto } from './users.dto';


@Controller('users')
    @AuthGuard([UserRole.administrator])
export class UsersController {
    constructor(@Inject('usersQuery') private readonly userQuery: UsersQueryservice) { }


    @Get('whoami')
    async whoami(
        @CurrentUser() user: IUser,
    ) {
        return user;
    }

    @Get('all')
    async getAllUsers() {
        return this.userQuery.list({
            skip: 0,
            orderProp: 'uuid',
            orderType: 'DESC',
            limit: 0,
            match: {} as IUser,
        })
    }

    @Get('list')
    async getList(
        @Query('limit', new ParseIntPipe()) limit: number,
        @Query('skip', new ParseIntPipe()) skip: number,
    ) {
        return this.userQuery.list({
            skip,
            orderProp: 'uuid',
            orderType: 'DESC',
            limit,
            match: {} as IUser,
        })
    }


    @Post()
    async create(@Body() user: CreateUserDto) {
        const res = await this.userQuery.list({
            skip: 0,
            orderProp: 'uuid',
            orderType: 'DESC',
            limit: 1,
            match: { login: user.login },
        });
        if (res.length) {
            throw new ConflictException("User with this login already exists");
        }
        user.password = md5(user.password);
        await this.userQuery.create<1>([{
            uuid: uuid.v4(),
            role: UserRole.client,
            ...user,
        }])
    }

    @Put()
    async update(@Body() user: UpdateUserDto) {
        if (user.password) {
            user.password = md5(user.password);
        }
        const restUser = await this.userQuery.list({
            skip: 0,
            orderProp: 'uuid',
            orderType: 'DESC',
            limit: 1,
            match: { uuid: user.uuid } as IUser,
        })
        const res = await this.userQuery.update<1>([{ ...restUser, ...user }])
        delete res[0].password;
        return res;
    }

    @Delete(':id')
    async delete(@Param('id', new ParseUUIDPipe({ version: '4' })) uuid: string) {
        return this.userQuery.remove([uuid]);
    }
}
