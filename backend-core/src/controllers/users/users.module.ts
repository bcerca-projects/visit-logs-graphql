
import { Inject, Module } from '@nestjs/common';
import { AuthCoreModule } from '../../auth/authCore.module';
import { DatabaseDriverEnum } from '../../config';
import { QueriesModule } from '../../queries/queries.module';
import { UsersController } from './users.controller';


@Module({
    imports: [QueriesModule.registrate(DatabaseDriverEnum.neo4j), AuthCoreModule],
    controllers: [UsersController],
    providers: [],
})
export class UsersControllerModule {
    constructor() {
    }
}
