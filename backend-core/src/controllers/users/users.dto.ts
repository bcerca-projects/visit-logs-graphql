import { IUser } from "@shared/models/users/IUser";
import { UserRole } from "@shared/models/users/UserRole";
import { IsEmpty, IsNotEmpty, IsString, IsEmail, IsOptional, IsEnum, IsUUID } from "class-validator";

export class CreateUserDto implements IUser {
    @IsEmpty()
    uuid: string;

    @IsNotEmpty()
    @IsString()
    @IsEmail()
    login: string;

    @IsNotEmpty()
    @IsString()
    password: string;

    @IsOptional()
    @IsString()
    @IsEnum(UserRole)
    role: UserRole;
}

export class UpdateUserDto implements IUser {
    @IsNotEmpty()
    @IsUUID('4')
    uuid: string;

    @IsOptional()
    @IsString()
    login: string;

    @IsOptional()
    @IsString()
    password: string;

    @IsOptional()
    @IsString()
    @IsEnum(UserRole)
    // @IsEmpty()
    role: UserRole;
}