
import { Module } from '@nestjs/common';
import { AuthCoreModule } from '../../auth/authCore.module';
import { DatabaseDriverEnum } from '../../config';
import { QueriesModule } from '../../queries/queries.module';
import { AuthController } from './auth.controller';
import { MailerModule } from 'mailer/mailer.module';


@Module({
    imports: [QueriesModule.registrate(DatabaseDriverEnum.neo4j), AuthCoreModule, MailerModule],
    controllers: [AuthController],
    providers: [],
})
export class AuthControllerModule {
    constructor() {
    }
}
