
import { Body, ConflictException, Controller, Inject, NotFoundException, Post, UnauthorizedException } from '@nestjs/common';
import * as uuid from 'uuid';
import { ITokens } from '@shared/auth/ITokens';
import { UserRole } from '@shared/models/users/UserRole';
import * as md5 from 'md5';
import { AuthService } from '../../auth/auth.service';
import { UsersQueryservice } from '../../queries/neo4j/users/usersQuery.service';
import { config } from 'config';
import { MailerService } from 'mailer/mailer.service';
import { LoginUserDto, TokenDto, DiscardPassword } from './auth.dto';

@Controller('auth')
export class AuthController {
    constructor
        (
        @Inject('usersQuery') private readonly userQuery: UsersQueryservice,
        private readonly auth: AuthService,
        private readonly mailer: MailerService,
    ) { }

    @Post('login')
    async login(@Body() user: LoginUserDto): Promise<ITokens> {
        user.password = md5(user.password);

        const res = await this.userQuery.list({
            skip: 0,
            orderProp: 'uuid',
            orderType: 'DESC',
            limit: 1,
            match: user,
        });
        if (!res.length) {
            throw new NotFoundException();
        }

        if (!await this.auth.validateUser(res[0].uuid, user.password)) {
            throw new UnauthorizedException();
        }
        const tokens = await this.auth.login(res[0]);
        if (!tokens) {
            throw new UnauthorizedException();
        }

        return tokens;
    }

    @Post('refresh')
    async refresh(@Body() tokens: TokenDto): Promise<ITokens> {
        const refreshed = await this.auth.refreshToken(tokens);
        if (!refreshed) {
            throw new UnauthorizedException();
        }
        return refreshed;
    }

    @Post('registrate')
    async registrate(@Body() user: LoginUserDto) {
        const res = await this.userQuery.list({
            skip: 0,
            orderProp: 'uuid',
            orderType: 'DESC',
            limit: 1,
            match: { login: user.login },
        });
        if (res.length) {
            throw new ConflictException("User with this login already exists");
        }
        user.password = md5(user.password);
        await this.userQuery.create<1>([{
            uuid: uuid.v4(),
            role: UserRole.client,
            ...user,
        }])
    }


    @Post('discard')
    async discard(@Body() user: DiscardPassword) {
        const res = await this.userQuery.list({
            skip: 0,
            orderProp: 'uuid',
            orderType: 'DESC',
            limit: 1,
            match: { login: user.login },
        });
        if (!res.length) {
            throw new NotFoundException();
        }
        const _user = res[0];
        const tokens = await this.auth.login(res[0], { accessTokenLifetime: config().discardTokenLifetime, refreshTokenLifetime: 0 });

        this.mailer.mail({
            to: _user.login,
            subject: "Account recovery",
            html: `${config().recoveryAccountPath}/?atoken=${tokens.accessToken}`
        })
    }

}
