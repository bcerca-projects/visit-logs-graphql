import { ITokens } from "@shared/auth/ITokens";
import { IUser } from "@shared/models/users/IUser";
import { UserRole } from "@shared/models/users/UserRole";
import { IsEmpty, IsNotEmpty, IsString, IsEmail, IsOptional, IsEnum } from "class-validator";

export class LoginUserDto implements IUser {
    @IsEmpty()
    uuid: string;

    @IsNotEmpty()
    @IsString()
    @IsEmail()
    login: string;

    @IsNotEmpty()
    @IsString()
    password: string;

    @IsOptional()
    @IsString()
    @IsEnum(UserRole)
    role: UserRole;
}


export class DiscardPassword implements IUser {
    @IsNotEmpty()
    @IsString()
    @IsEmail()
    login: string;
}

export class TokenDto implements ITokens {
    @IsNotEmpty()
    @IsString()
    accessToken: string;

    @IsNotEmpty()
    @IsString()
    refreshToken: string;
}