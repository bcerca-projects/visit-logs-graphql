import { JwtModule, } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test } from '@nestjs/testing';
import { UserRole } from '@shared/models/users/UserRole';
import { config, DatabaseDriverEnum } from '../config';
import { QueriesModule } from '../queries/queries.module';
import { v4 } from 'uuid';
import { AuthService } from './auth.service';
import { jwtConstants } from './constants';
import { IPayload } from './IPayload';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from './local.strategy';
import { ConfigModule } from '@nestjs/config';


describe('CatsController', () => {
    let authService: AuthService;
    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    load: [config],
                }),
                QueriesModule.registrate(DatabaseDriverEnum.neo4j),
                PassportModule.register({ defaultStrategy: 'jwt' }),
                JwtModule.register({
                    secret: jwtConstants.secret,
                    signOptions: { expiresIn: '60s' },
                })],
            providers: [AuthService, LocalStrategy, JwtStrategy],
        }).compile();
        authService = moduleRef.get<AuthService>(AuthService);
    });
    describe('login', () => {
        it('should login a user', async () => {
            const user: IPayload = {
                uuid: v4(),
                login: 'user',
                role: UserRole.super,
            };
            const result = await authService.login(user);
            expect(result).toHaveProperty(['accessToken']);
            expect(result).toHaveProperty(['refreshToken']);
        });
    });

});
