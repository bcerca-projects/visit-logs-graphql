import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IPayload } from './IPayload';
import { IUser } from '@shared/models/users/IUser';
import { ITokens } from '@shared/auth/ITokens';

import { Inject } from '@nestjs/common';
import { UsersQueryservice } from '../queries/neo4j/users/usersQuery.service';
import { config } from 'config';

@Injectable()
export class AuthService {
    constructor(
        @Inject('usersQuery') private readonly userQuery: UsersQueryservice,
        private jwtService: JwtService
    ) { }

    async validateUser(uuid: string, pass: string): Promise<IUser> {
        const user = (await this.userQuery.list({
            skip: 0,
            orderProp: 'uuid',
            orderType: 'DESC',
            limit: 0,
            match: { uuid } as IUser,
        }))[0];
        if (!(user && (user.password === pass))) {
            return;
        }
        const { password, ...result } = user;
        return result;
    }

    async login(user: IUser, options?: {
        /** in seconds */
        accessTokenLifetime?: number,
        /** in seconds */
        refreshTokenLifetime?: number,
    }): Promise<ITokens> {
        const { accessTokenLifetime = 86400, refreshTokenLifetime = 86400 } = options || {};
        const payload: IPayload = { uuid: user.uuid, login: user.login, role: user.role };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: accessTokenLifetime,
            secret: config().accessTokenSecret,
        });
        const refreshToken = this.jwtService.sign(payload, {
            expiresIn: refreshTokenLifetime,
            secret: accessToken,
        });
        this.userQuery.update([{ ...user, refreshToken }]);
        return { accessToken, refreshToken };
    }


    async refreshToken(tokens: ITokens): Promise<ITokens> {
        let payload: IPayload;
        try {
            payload = this.jwtService.verify<IPayload>(tokens.refreshToken, { secret: tokens.accessToken });
        }
        catch {
            return;
        }
        if (!payload) {
            return;
        }

        const users = await this.userQuery.list({
            skip: 0,
            orderProp: 'uuid',
            orderType: 'DESC',
            limit: 1,
            match: { uuid: payload.uuid } as IUser,
        });
        if (!users.length) {
            return;
        }
        const user = users[0];
        if (user.refreshToken !== tokens.refreshToken) {
            return;
        }
        return this.login(user);
    }
}
