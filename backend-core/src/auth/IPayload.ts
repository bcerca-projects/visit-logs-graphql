import { UserRole } from "@shared/models/users/UserRole";

export interface IPayload {
    uuid: string;
    login: string;
    role: UserRole;
}