import { Injectable } from '@nestjs/common';
import { config } from 'config';
import * as nodemailer from 'nodemailer'

@Injectable()
export class MailerService {
    private readonly transporter: nodemailer.Transporter = nodemailer.createTransport(
        config().mailer
    );

    constructor() {

    }

    async mail(options: { to: string, subject: string, html: string }) {
        const { to, subject, html } = options;
        await this.transporter.sendMail({
            from: `Service <${config().mailer.auth.user}>`,
            to,
            subject,
            html,
        })
    }
}
