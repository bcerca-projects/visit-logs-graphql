import { Inject, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { config, DatabaseDriverEnum } from './config';
import { IDataBaseService } from '@shared/database/IDataBaseService';
import { UsersControllerModule } from './controllers/users/users.module';
import { QueriesModule, } from './queries/queries.module';
import { AuthControllerModule } from './controllers/auth/auth.module';
import { catchError, from, of, retry } from 'rxjs';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ActivityLogsGraphQLModule } from 'activity-logs/activity-logs.module';

@Module({
  imports: [
    ActivityLogsGraphQLModule,

    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      include: [ActivityLogsGraphQLModule],
      autoSchemaFile: true,
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    UsersControllerModule,
    AuthControllerModule,
    QueriesModule.registrate(DatabaseDriverEnum.neo4j),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(@Inject('database') database: IDataBaseService) {
    from(database.connect())
      .pipe(
        retry(5),
        catchError((err) => of(console.error('Could not connect to db', err))),
      )
      .subscribe();
  }
}
