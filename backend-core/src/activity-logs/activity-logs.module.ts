
import {  Module } from '@nestjs/common';
import { AuthCoreModule } from 'auth/authCore.module';
import { DatabaseDriverEnum } from 'config';
import { QueriesModule } from 'queries/queries.module';
import { VisitLogResolver } from './resolvers/Visit.resolver';


@Module({
    imports: [QueriesModule.registrate(DatabaseDriverEnum.neo4j), AuthCoreModule],
    controllers: [],
    providers: [VisitLogResolver],
})
export class ActivityLogsGraphQLModule {
    constructor() {
    }
}
