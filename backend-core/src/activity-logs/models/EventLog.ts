import { Field, ID, ObjectType } from '@nestjs/graphql';
import { EventLogModels } from '@shared/models/activity-logs'

@ObjectType()
export class EventLog implements EventLogModels.IEventLog {
    @Field(type => ID)
    uid: string;

    @Field({ nullable: true })
    tag: EventLogModels.EventLogTag;

}