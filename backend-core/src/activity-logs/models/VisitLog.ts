import { Field, ID, ObjectType } from '@nestjs/graphql';
import { VisitLogModels } from '@shared/models/activity-logs'
import { EventLog } from './EventLog';

@ObjectType()
export class VisitLog implements VisitLogModels.IVisitLog {
    @Field(type => ID)
    uid: string;
    @Field({ nullable: true })
    reg_time: string;
    @Field({ nullable: true })
    fc_imp_chk: number;
    @Field({ nullable: true })
    fc_time_chk: number;
    @Field({ nullable: true })
    utmtr: number;
    @Field({ nullable: true })
    mm_dma: number;
    @Field({ nullable: true })
    osName: string;
    @Field({ nullable: true })
    model: string;
    @Field({ nullable: true })
    hardware: string;
    @Field({ nullable: true })
    site_id: string;

    @Field(type => [EventLog])
    events: EventLog[];
}