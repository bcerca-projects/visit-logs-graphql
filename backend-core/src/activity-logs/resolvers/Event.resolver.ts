import { Inject, NotFoundException } from "@nestjs/common";
import { Args, ID, Mutation, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { VisitLog } from "../models/VisitLog";
import { IEventLogsQuery,  } from "queries/IQueryList";
import { EventLogInput, VisitLogInput } from "./input-types";
import { v4 } from "uuid";
import { EventLog } from "activity-logs/models/EventLog";
import { IEventLog } from "@shared/models/activity-logs/events";
import { FixedArray } from "@shared/types/FixedArray";



@Resolver(of => EventLog)
export class EventLogResolver {
    constructor(
        @Inject('eventLogsQuery') private readonly eventLogsQueryService: IEventLogsQuery,
    ) { }

    @Query(returns => EventLog)
    async visitLog(@Args('uid', { type: () => ID }) uid: string) {
        const events = await this.eventLogsQueryService.list({
            skip: 0,
            orderProp: 'uid',
            orderType: 'DESC',
            limit: 1,
            match: { uid },
        })
        if (!events.length) {
            throw new NotFoundException();
        }
        const event = events[0];

        return event;
    }


    @Mutation(returns => EventLog)
    async eventLogInput(
        @Args('eventLogInput') eventLogInput: EventLogInput,
    ) {
        let events: FixedArray<IEventLog, 1>;
        let uid = eventLogInput.uid
        let parent_uid = eventLogInput.parent_uid;
        if (!uid) {
            uid = v4()
            events = await this.eventLogsQueryService.create<1>([{ ...eventLogInput, uid }],parent_uid);
        } else {
            events = await this.eventLogsQueryService.update<1>([{ ...eventLogInput, uid }]);
        }
        if (!events.length) {
            throw new NotFoundException();
        }
        const event = events[0];
        return event;
    }


}