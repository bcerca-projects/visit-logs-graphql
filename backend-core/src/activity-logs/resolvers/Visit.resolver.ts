import { Inject, NotFoundException } from "@nestjs/common";
import { Args, ID,  Int,  Mutation, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { VisitLog } from "../models/VisitLog";
import { IEventLogsQuery, IVisitLogsQuery } from "queries/IQueryList";
import { FetchVisits, VisitLogInput } from "./input-types";
import { v4 } from "uuid";



@Resolver(of => VisitLog)
export class VisitLogResolver {
    constructor(
        @Inject('visitLogsQuery') private readonly visitService: IVisitLogsQuery,
        @Inject('eventLogsQuery') private readonly eventLogsQueryService: IEventLogsQuery,
    ) { }

    @Query(returns => [VisitLog])
    async visitLogList(@Args() args: FetchVisits) {
        const visits = await this.visitService.list({
            skip: args.skip,
            orderProp: 'uid',
            orderType: 'DESC',
            limit: args.take,
            match: {},
        })
        if (!visits.length) {
            throw new NotFoundException();
        }
        visits.map(visit => visit.events = [])
        return visits;
    }

    @Query(returns => VisitLog)
    async visitLog(@Args('uid', { type: () => ID }) uid: string) {
        const visits = await this.visitService.list({
            skip: 0,
            orderProp: 'uid',
            orderType: 'DESC',
            limit: 1,
            match: { uid },
        })
        if (!visits.length) {
            throw new NotFoundException();
        }
        const visit = visits[0];
        const events = await this.eventLogsQueryService.listByVisit({
            skip: 0,
            orderProp: 'uid',
            orderType: 'DESC',
            limit: 1,
            match: { uid },
        });
        visit.events = events ?? []
        return visit;
    }

    @ResolveField()
    async events(@Parent() visitLog: VisitLog) {
        const { uid } = visitLog;
        return this.eventLogsQueryService.list({
            skip: 0,
            orderProp: 'uid',
            orderType: 'DESC',
            limit: 100,
            match: { uid },
        })
    }


    @Mutation(returns => VisitLog)
    async visitLogInput(
        @Args('visitLogInput') visitLogInput: VisitLogInput,
    ) {
        const uid = visitLogInput.uid ?? v4();
        const visits = visitLogInput.uid ?
            await this.visitService.update<1>([{ ...visitLogInput, uid }])
            : await this.visitService.create<1>([{ ...visitLogInput, uid }]);
        if (!visits.length) {
            throw new NotFoundException();
        }
        const visit = visits[0];
        const events = await this.eventLogsQueryService.listByVisit({
            skip: 0,
            orderProp: 'uid',
            orderType: 'DESC',
            limit: 1,
            match: { uid: visit.uid },
        });
        visit.events = events ?? []
        return visit;
    }


}