import { InputType, Field, ArgsType, Int } from '@nestjs/graphql';
import { EventLogModels, VisitLogModels } from '@shared/models/activity-logs';
import { Min, Max } from 'class-validator';

@InputType()
export class VisitLogInput implements Omit<VisitLogModels.IVisitLog, 'uid'> {
  @Field({ nullable: true })
  uid?: string;

  @Field({ nullable: true })
  reg_time?: string;
  @Field({ nullable: true })
  fc_imp_chk?: number;
  @Field({ nullable: true })
  fc_time_chk?: number;
  @Field({ nullable: true })
  utmtr?: number;
  @Field({ nullable: true })
  mm_dma?: number;
  @Field({ nullable: true })
  osName?: string;
  @Field({ nullable: true })
  model?: string;
  @Field({ nullable: true })
  hardware?: string;
  @Field({ nullable: true })
  site_id?: string;
}

@InputType()
export class EventLogInput implements Omit<EventLogModels.IEventLog, 'uid'> {
  @Field({ nullable: true })
  uid?: string;

  @Field({ nullable: false })
  parent_uid: string;


  @Field({ nullable: true })
  tag?: EventLogModels.EventLogTag
}

@ArgsType()
export class FetchVisits {
  @Field(() => Int)
  @Min(0)
  skip = 0

  @Field(() => Int)
  @Min(1)
  @Max(50)
  take = 25
}