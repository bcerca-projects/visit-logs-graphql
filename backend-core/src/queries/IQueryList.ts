import { IUser } from '@shared/models/users/IUser';

import { FixedArray } from '@shared/types/FixedArray';
import { Type } from '@nestjs/common';
import { IDataBaseService } from '@shared/database/IDataBaseService';
import { IFilterQueries } from './IFilterQueries';
import { EventLogModels, VisitLogModels } from '@shared/models/activity-logs';
import { IVisitLog } from '@shared/models/activity-logs/visits';

export interface IUserQuery<T extends IUser = IUser> {
    create<N extends number>(users: FixedArray<T, N>): Promise<FixedArray<T, N>>,
    update<N extends number>(users: FixedArray<T, N>): Promise<FixedArray<T, N>>,
    remove(uuid: string[]): Promise<boolean>,
    list(filter: IFilterQueries<T>): Promise<T[]>,
}

export interface IVisitLogsQuery<T extends VisitLogModels.IVisitLog = VisitLogModels.IVisitLog> {
    create<N extends number>(visits: FixedArray<T, N>): Promise<FixedArray<T, N>>,
    update<N extends number>(visits: FixedArray<T, N>): Promise<FixedArray<T, N>>,
    remove(uuid: string[]): Promise<boolean>,
    list(filter: IFilterQueries<T>): Promise<T[]>,
}


export interface IEventLogsQuery<T extends EventLogModels.IEventLog = EventLogModels.IEventLog> {
    create<N extends number>(visits: FixedArray<T, N>, parent_uid: string): Promise<FixedArray<T, N>>,
    update<N extends number>(visits: FixedArray<T, N>): Promise<FixedArray<T, N>>,
    remove(uuid: string[]): Promise<boolean>,
    list(filter: IFilterQueries<T>): Promise<T[]>,
    listByVisit(filter: IFilterQueries<IVisitLog>): Promise<T[]>,
}


export interface IQueryList {
    database: Type<IDataBaseService>;
    users?: Type<IUserQuery>;
    visitLogs?: Type<IVisitLogsQuery>;
    eventLogs?: Type<IEventLogsQuery>;
}