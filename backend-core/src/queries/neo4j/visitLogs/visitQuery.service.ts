import { Inject, Injectable } from '@nestjs/common';
import { FixedArray } from '@shared/types/FixedArray';
import { IFilterQueries } from '../../../queries/IFilterQueries';
import { Neo4jDatabaseService } from '../database/neo4jDatabase.service';
import { IVisitLog } from '@shared/models/activity-logs/visits';
import { IVisitLogsQuery } from  '../../../queries/IQueryList';

@Injectable()
export class VisitLogsQueryservice
    implements IVisitLogsQuery<IVisitLog> {
    constructor(@Inject('database') private readonly database: Neo4jDatabaseService) {

    }

    create<N extends number>(VisitLogs: FixedArray<IVisitLog, N>): Promise<FixedArray<IVisitLog, N>> {
        return this.database.createNodes<IVisitLog, N>('VisitLog', VisitLogs);
    }

    update<N extends number>(VisitLogs: FixedArray<IVisitLog, N>): Promise<FixedArray<IVisitLog, N>> {
        return this.database.updateNodes<IVisitLog, N>('VisitLog', VisitLogs, 'uid');
    }

    remove(uuid: string[]): Promise<boolean> {
        return this.database.deleteNodes('VisitLog', uuid.map(u => ({ uuid: u })));
    }

    list(filter: IFilterQueries<IVisitLog>): Promise<IVisitLog[]> {
        return this.database.matchNodes<IVisitLog>({
            limit: filter.limit, category: 'VisitLog', order_by: filter.orderProp, skip: filter.skip
        }, filter.match);
    }

}
