import * as neo4j from 'neo4j-driver';
import { FixedArray } from '@shared/types/FixedArray'
import { InternalServerErrorException } from '@nestjs/common';
import { v4 } from 'uuid';

export enum RelationConnectionTypes {
    toLeft = '<--',
    toRight = '-->',
    bothSides = '<-->'
}

export class neo4jQueriyBuilder<Entity extends object = any> {

    private _limit: string = '';

    private _skip: string = '';

    private _orderBy: string = '';

    private _match: string;

    private _params: any = {};

    private _nodes: string[] = [];

    get query() {
        return {
            query: `
                MATCH ${this._match}
                RETURN ${this._nodes.toString()}
                ${this._orderBy}
                ${this._skip}
                ${this._limit}
            `, params: this._params
        }
    }

    constructor(private readonly session: neo4j.Session) {
        this._match = '';
    }

    matchNode<Entity extends object>(category: string, object: Entity = {} as Entity): this {
        const keys = Object.keys(object);

        const uuid = `a${v4().replace('-', '').replace('-', '').replace('-', '').replace('-', '')}`;
        Object.values(object).forEach((p, i) => {
            this._params[`${uuid}${i}`] = p;
        })
        this._match += (`  (p${uuid}: ${category} { ${keys.map((k, i) => `${k}: $${uuid}${i}`).join(',')} })`);
        this._nodes.push(`p${uuid}`);
        return this;
    }

    matchRelation(type: string, connection: RelationConnectionTypes): this {
        switch (connection) {
            case RelationConnectionTypes.toRight: {
                this._match += `-[:${type}]->`
                break;
            }
            case RelationConnectionTypes.toLeft: {
                this._match += `<-[:${type}]-`
                break;
            }
            case RelationConnectionTypes.bothSides: {
                this._match += `<-[:${type}]->`
                break;
            }
        }
        return this;
    }

    async match(): Promise<any> {
        const result = await new Promise<Entity[]>((res, rej) => {
            this.session.run(
                `
                MATCH ${this._match}
                RETURN ${this._nodes.toString()}
                ${this._orderBy}
                ${this._skip}
                ${this._limit}
            `, this._params
            ).then(result => {
                console.log(result)
                res(result.records.map(r => r.toObject()[this._nodes[0]].properties as Entity))
            })
                .catch(r => rej(r));
        })
        this.close();
        return result;
    }

    limit(limit: number): this {
        this._limit = limit ? `LIMIT ${limit}` : '';
        return this;
    }

    skip(skip: number): this {
        this._skip = skip ? `SKIP ${skip}` : '';
        return this;
    }

    orderBy(property: string): this {
        this._orderBy = property ? `ORDER BY ${this._nodes[0]}.${property}` : '';
        return this;
    }

    async createNodes<N extends number>(category: string, objects: FixedArray<Entity, N>): Promise<FixedArray<Entity, N>> {
        const transaction = this.session.beginTransaction();
        let result: FixedArray<Entity, N>;
        try {
            result = await (Promise.all(objects.map(object => {
                const keys = Object.keys(object);
                const parameters = {};
                Object.values(object).forEach((p, i) => {
                    parameters[`param${i}`] = p;
                });

                return new Promise<Entity>(async (res, rej) => {
                    await transaction.run(
                        `
                    CREATE (p: ${category} { ${keys.map((k, i) => `${k}: $param${i}`).join(',')} })
                    RETURN p
                `, parameters
                    ).then(result => {
                        res(result.records[0].toObject().p.properties as Entity)
                    })
                        .catch(r => rej(r));
                })
            }))) as FixedArray<Entity, N>;
            await transaction.commit();
        }
        catch (err) {
            transaction.rollback();
            throw new InternalServerErrorException(err, 'Transaction rollback');
        }

        return result;
    }

    async update<N extends number>(category: string, objects: FixedArray<Entity, N>, indentificationProperty: string): Promise<FixedArray<Entity, N>> {
        const transaction = this.session.beginTransaction();
        const result = await (Promise.all(objects.map(object => {
            const keys = Object.keys(object);
            const parameters = {};
            Object.values(object).forEach((p, i) => {
                parameters[`param${i}`] = p;
            });

            return new Promise<Entity>((res, rej) => {
                transaction.run(
                    `
                    MATCH (p: ${category} { ${indentificationProperty}: $indentificationProperty })
                    SET p = {${keys.map((k, i) => `${k}: $param${i}`).join(',')}}
                    RETURN p
                `, { ...parameters, indentificationProperty: object[indentificationProperty] }
                ).then(result => {
                    res(result.records[0].toObject().p.properties as Entity)
                })
                    .catch(r => rej(r));
            })
        })));
        await transaction.commit()
        return result as FixedArray<Entity, N>;
    }

    async remove<Entity extends object>(category: string, objects: Entity[]): Promise<boolean> {
        const transaction = this.session.beginTransaction();
        let result: Array<boolean>;
        try {
            result = await Promise.all(objects.map(async (object) => {
                const keys = Object.keys(object);

                const parameters = {};
                Object.values(object).forEach((p, i) => {
                    parameters[`param${i}`] = p;
                })
                return await new Promise<boolean>((res, rej) => {
                    transaction.run(
                        `
                    MATCH (p: ${category} { ${keys.map((k, i) => `${k}: $param${i}`).join(',')} })
                    DETACH DELETE p
                `, parameters
                    ).then(() => {
                        res(true)
                    })
                        .catch(r => rej(r));
                })
            }));
            await transaction.commit()
        } catch {
            await transaction.rollback();
        }

        return result.reduce((acc, cur) => acc && cur);
    }

    async createRelation<Entity1 extends object, Entity2 extends object>(category1: string, category2: string, object1: Entity1, object2: Entity2, relation: string): Promise<boolean> {
        const transaction = this.session.beginTransaction();

        const keys1 = Object.keys(object1);

        const parameters = {};

        Object.values(object1).forEach((p, i) => {
            parameters[`param1${i}`] = p;
        });

        const keys2 = Object.keys(object2);

        Object.values(object2).forEach((p, i) => {
            parameters[`param2${i}`] = p;
        });

        let promise;

        try {

            promise = await new Promise<any>((res, rej) => {
                transaction.run(`
            MATCH (a: ${category1} { ${keys1.map((k, i) => `${k}: $param1${i}`).join(',')} }),
                  (b: ${category2} { ${keys2.map((k, i) => `${k}: $param2${i}`).join(',')} })
            CREATE (a)-[r:${relation}]->(b)
            RETURN b
        `, parameters).then(r => res(r)).catch(r => { console.error(r); rej(r) });;
            });
            await transaction.commit()

        }
        catch {
            await transaction.rollback();

        }

        return promise;
    }


    close(): void {
        this.session.close();
    }

}