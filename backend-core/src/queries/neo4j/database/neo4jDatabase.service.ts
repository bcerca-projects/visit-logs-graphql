import { Injectable } from '@nestjs/common';
import * as neo4j from 'neo4j-driver';
import { IDataBaseService } from '@shared/database/IDataBaseService';
import { FixedArray } from '@shared/types/FixedArray';
import { neo4jQueriyBuilder } from './neo4jQueryBuilder';
import { ConfigService } from '@nestjs/config';
@Injectable()
export class Neo4jDatabaseService implements IDataBaseService {

    private readonly _driver: neo4j.Driver;

    private _session: neo4j.Session;

    public getSession(options?: {
        defaultAccessMode?: neo4j.SessionMode;
        bookmarks?: string | string[];
        database?: string;
        fetchSize?: number;
    }): neo4j.Session {
        return this._driver.session(options);
    }

    public get session(): neo4j.Session {
        return this._driver.session();
    }

    constructor(private readonly configService: ConfigService) {
        this._driver = neo4j.driver(this.configService.get('database.host'), neo4j.auth.basic(configService.get('database.login'), configService.get('database.password')));
    }

    public async connect(): Promise<this> {
        console.log('Using neo4j')
        console.log(this.configService.get('database.host'), neo4j.auth.basic(this.configService.get('database.login'), this.configService.get('database.password')))
        await this._driver.verifyConnectivity()
        this._session = this._driver.session()
        return this;
    }

    public query(query: {
        text: string;
        parameters?: any;
    }): Promise<neo4j.QueryResult> {
        return new Promise<neo4j.QueryResult>((res, rej) => {
            this._session.run(query)
                .then(result => {
                    res(result)
                })
                .catch(r => rej(r));
        })
    }

    async matchNodes<Entity extends object>(contraits: { order_by: string, skip: number, limit: number, category: string, }, object?: Entity): Promise<Array<Entity>> {
        const queryBuilder = new neo4jQueriyBuilder<Entity>(this.getSession());
        return queryBuilder.matchNode(contraits.category, object).limit(contraits.limit).skip(contraits.skip).orderBy(contraits.order_by).match();
    }

    async createNodes<Entity extends object, N extends number = any>(category: string, objects: FixedArray<Entity, N>): Promise<FixedArray<Entity, N>> {
        const queryBuilder = new neo4jQueriyBuilder<Entity>(this.getSession());
        const result = await queryBuilder.createNodes<N>(category, objects);
        queryBuilder.close();
        return result;
    }

    async createRelationship<Entity1 extends object, Entity2 extends object>(category1: string, category2: string, object1: Entity1, object2: Entity2, relation: string) {
        const queryBuilder = new neo4jQueriyBuilder<Entity1>(this.getSession());
        const result = await queryBuilder.createRelation(category1, category2, object1, object2, relation);
        queryBuilder.close();
        return result;
    }

    async updateNodes<Entity extends object, N extends number = any>(
        category: string,
        objects: FixedArray<Entity, N>,
        matchProperty: string,
    ): Promise<FixedArray<Entity, N>> {
        const queryBuilder = new neo4jQueriyBuilder<Entity>(this.getSession());
        const result = await queryBuilder.update<N>(category, objects, matchProperty);
        queryBuilder.close();
        return result;
    }

    async deleteNodes<Entity extends object>(
        category: string,
        objects: Entity[],
    ): Promise<boolean> {
        const queryBuilder = new neo4jQueriyBuilder(this.getSession());
        const result = await queryBuilder.remove(category, objects);
        queryBuilder.close();
        return result;
    }

    public destroy() {
        this._driver.close()
    }
}
