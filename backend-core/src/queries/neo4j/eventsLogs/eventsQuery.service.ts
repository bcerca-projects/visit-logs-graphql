import { Inject, Injectable } from '@nestjs/common';
import { FixedArray } from '@shared/types/FixedArray';
import { IFilterQueries } from '../../IFilterQueries';
import { Neo4jDatabaseService } from '../database/neo4jDatabase.service';
import { IEventLogsQuery,  } from  '../../IQueryList';
import { IEventLog } from '@shared/models/activity-logs/events';
import { RelationConnectionTypes, neo4jQueriyBuilder } from '../database/neo4jQueryBuilder';
import { IVisitLog } from '@shared/models/activity-logs/visits';

@Injectable()
export class EventLogsQueryService
    implements IEventLogsQuery {
    constructor(@Inject('database') private readonly database: Neo4jDatabaseService) {

    }

    async create<N extends number>(eventLogs: FixedArray<IEventLog, N>, parent_uid: string): Promise<FixedArray<IEventLog, N>> {
        const queryBuilder = new neo4jQueriyBuilder<IEventLog>(this.database.getSession());
        const result = await queryBuilder.createNodes<N>('EventLog', eventLogs);
        result.forEach(eventLog => {
            queryBuilder.createRelation('EventLog', 'VisitLog',eventLog,{parent_uid}, 'parent')
        });
        return result
    }

    update<N extends number>(EventLogs: FixedArray<IEventLog, N>): Promise<FixedArray<IEventLog, N>> {
        return this.database.updateNodes<IEventLog, N>('EventLog', EventLogs, 'uid');
    }

    remove(uuid: string[]): Promise<boolean> {
        return this.database.deleteNodes('EventLog', uuid.map(u => ({ uuid: u })));
    }

    list(filter: IFilterQueries<IEventLog>): Promise<IEventLog[]> {
        const queryBuilder = new neo4jQueriyBuilder<IEventLog>(this.database.getSession());
        return queryBuilder
            .matchNode('EventLog', filter.match)
            .limit(filter.limit)
            .skip(filter.skip)
            .orderBy(filter.orderProp)
            .match();

    }

    listByVisit(filter: IFilterQueries<IVisitLog>): Promise<IEventLog[]> {
        const queryBuilder = new neo4jQueriyBuilder<IEventLog>(this.database.getSession());
        return queryBuilder
            .matchNode('EventLog')
            .limit(filter.limit)
            .skip(filter.skip)
            .orderBy(filter.orderProp)
            .matchRelation('parent', RelationConnectionTypes.toLeft)
            .matchNode('VisitLog', filter.match)
            .match();

    }


}
