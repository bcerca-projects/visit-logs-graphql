import { Inject, Injectable } from '@nestjs/common';
import { IUser } from '@shared/models/users/IUser';
import { FixedArray } from '@shared/types/FixedArray';
import { IFilterQueries } from '../../../queries/IFilterQueries';
import { IUserQuery } from '../../../queries/IQueryList';
import { Neo4jDatabaseService } from '../database/neo4jDatabase.service';

@Injectable()
export class UsersQueryservice
    implements IUserQuery<IUser> {
    constructor(@Inject('database') private readonly database: Neo4jDatabaseService) {

    }

    create<N extends number>(users: FixedArray<IUser, N>): Promise<FixedArray<IUser, N>> {
        return this.database.createNodes<IUser, N>('User', users);
    }

    update<N extends number>(users: FixedArray<IUser, N>): Promise<FixedArray<IUser, N>> {
        return this.database.updateNodes<IUser, N>('User', users, 'uuid');
    }

    remove(uuid: string[]): Promise<boolean> {
        return this.database.deleteNodes('User', uuid.map(u => ({ uuid: u })));
    }

    list(filter: IFilterQueries<IUser>): Promise<IUser[]> {
        return this.database.matchNodes<IUser>({
            limit: filter.limit, category: 'User', order_by: filter.orderProp, skip: filter.skip
        }, filter.match);
    }

}
