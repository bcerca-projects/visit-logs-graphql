import { Type } from '@nestjs/common';
import { IEventLogsQuery, IQueryList, IUserQuery, IVisitLogsQuery } from '../IQueryList';
import { Neo4jDatabaseService } from './database/neo4jDatabase.service';
import { UsersQueryservice } from './users/usersQuery.service';
import { EventLogsQueryService } from './eventsLogs/eventsQuery.service';
import { VisitLogsQueryservice } from './visitLogs/visitQuery.service';

interface INeo4jQueryList extends IQueryList {
    eventLogs: Type<IEventLogsQuery>,
    visitLogs: Type<IVisitLogsQuery>,
    users: Type<IUserQuery>,
}

export const neo4jQueries: INeo4jQueryList = {
    users: UsersQueryservice,
    database: Neo4jDatabaseService,
    eventLogs: EventLogsQueryService,
    visitLogs: VisitLogsQueryservice,
};
