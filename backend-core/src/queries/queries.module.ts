import { DynamicModule, Module, Provider, Type } from '@nestjs/common';
import { DatabaseDriverEnum } from '../config';
import { IQueryList } from './IQueryList';
import { neo4jQueries } from './neo4j/neo4jQueries';

@Module({})
export class QueriesModule {
    static registrate(driver: DatabaseDriverEnum): DynamicModule {
        let queries: IQueryList;
        const services: Provider[] = [];
        switch (driver) {
            case DatabaseDriverEnum.neo4j: {
                queries = neo4jQueries;
                break;
            }
        }
        services.push({
            provide: 'database',
            useClass: queries.database,
        });
        services.push({
            provide: 'usersQuery',
            useClass: queries.users,
        });

        services.push({
            provide: 'visitLogsQuery',
            useClass: queries.visitLogs,
        });

        services.push({
            provide: 'eventLogsQuery',
            useClass: queries.eventLogs,
        });

        return {
            module: QueriesModule,
            providers: services,
            exports: services,
        };
    }
}
