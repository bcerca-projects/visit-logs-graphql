
export interface IFilterQueries<T> {
    skip: number,
    orderProp: string,
    orderType: 'DESC' | 'ASC',
    limit: number,
    match: T,
}