export enum DatabaseDriverEnum {
    neo4j = 'neo4j',
}

export interface IConfig {
    port: number,
    god_mode: boolean
    accessTokenSecret: string
    discardTokenLifetime: number
    recoveryAccountPath: string
    mailer: {
        host: string,
        port: number,
        tls: {
            rejectUnauthorized: boolean,
            minVersion: "TLSv1.2"
        },
        auth: {
            user: string,
            pass: string,
        }
    }
    database: {
        driver: DatabaseDriverEnum,
        host: string,
        port: number,
        password: string,
        login: string,
    }
}

export const config = (): IConfig => ({
    port: parseInt(process.env.PORT, 10) || 3000,
    god_mode: !!parseInt(process.env.GOD_MODE),
    accessTokenSecret: process.env.ACCESS_TOKEN_SECRET || 'ACCESS_TOKEN_SECRET',
    discardTokenLifetime: parseInt(process.env.DISCARD_TOKEN_LIFETIME) || 3600,
    recoveryAccountPath: process.env.RECOVERY_PATH,
    database: {
        driver: process.env.DRIVER as DatabaseDriverEnum || DatabaseDriverEnum.neo4j,
        host: process.env.DATABASE_HOST || 'neo4j://localhost',
        login: process.env.DATABASE_LOGIN || 'neo4j',
        password: process.env.DATABASE_PASSWORD || '',
        port: parseInt(process.env.DATABASE_PORT, 10) || 5432
    },
    mailer: {
        host: process.env.MAILER_SMPT_HOST,
        port: parseInt(process.env.MAILER_PORT) || 465,
        tls: {
            rejectUnauthorized: true,
            minVersion: "TLSv1.2"
        },
        auth: {
            user: process.env.EMAIL_SENDER_USER,
            pass: process.env.EMAIL_SENDER_PASSWORD
        }
    }
});
